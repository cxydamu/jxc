package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author damu
 * @date 22.12.30 16:18
 */
public interface SupplierDao {

    /**
     * 根据供应商名称查询供应商
     * @return
     */
    List<Supplier> selectBySupplierName(@Param("supplierName") String supplierName);


    /**
     * 供应商添加
     * @param supplier
     * @return
     */
    Integer insert(Supplier supplier);


    /**
     * 根据id查询对应供应商
     * @param supplierId
     * @return
     */
    Supplier selectById(@Param("supplierId") Integer supplierId);


    /**
     * 供应商修改
     * @param oldSupplier
     * @return
     */
    Integer updateById(Supplier oldSupplier);


    /**
     * 删除供应商（支持批量删除）
     * @param ids
     * @return
     */
    Integer deleteByIds(@Param("ids") String ids);
}
