package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 库存管理
 * @author damu
 * @date 23.1.3 16:50
 */
@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListDao damageListDao;

    @Autowired
    private DamageListGoodsDao damageListGoodsDao;


    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsList
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServiceVO saveDamageList(DamageList damageList, List<DamageListGoods> damageListGoodsList) {
        // 添加商品报损单
        damageListDao.insert(damageList);
        // 添加商品报损单商品列表
        for (DamageListGoods damageListGoods : damageListGoodsList) {
            // 完善商品报损单id
            damageListGoods.setDamageListId(damageList.getDamageListId());
            // 添加商品报损单商品
            damageListGoodsDao.insert(damageListGoods);
        }
        // 返回成功
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }



}
