package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;

import java.util.List;

/**
 * @author damu
 * @date 23.1.3 08:53
 */
public interface GoodsTypeService {

    /**
     * 查询商品所有分类
     * @return
     */
    List<GoodsTypeVo> getAllGoodsTypeTree();

    /**
     * 新增商品类别
     * @param goodsType
     */
    ServiceVO saveGoodsType(GoodsType goodsType)  throws Exception;


    /**
     * 删除商品类别
     * @param goodsTypeId
     * @return
     */
    ServiceVO deleteGoodsType(Integer goodsTypeId);



}
