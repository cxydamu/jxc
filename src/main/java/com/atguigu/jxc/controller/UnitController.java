package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 单位管理
 * @author damu
 * @date 23.1.3 08:59
 */
@RestController
@RequestMapping("/unit")
public class UnitController {

    @Autowired
    private UnitService unitService;


    /**
     * 查询所有商品单位
     * 请求URL：http://localhost:8080/unit/list
     * 请求参数：无
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("/list")
    public Map<String, Object> getUnitList() {
        return unitService.getUnitList();
    }


    /**
     * 添加商品单位
     * 请求URL：http://localhost:8080/unit/save
     * 请求参数：Unit unit
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO saveUnit(Unit unit){
        return unitService.saveUnit(unit);
    }

    /**
     * 根据id删除商品单位
     * 请求URL：http://localhost:8080/unit/delete
     * 请求参数：Integer unitId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/delete")
    public ServiceVO deleteUnit(Integer unitId){
        return unitService.deleteUnit(unitId);
    }


}
