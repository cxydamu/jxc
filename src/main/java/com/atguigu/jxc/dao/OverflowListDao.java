package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;

/**
 * 商品报溢单管理
 * @author damu
 * @date 23.1.3 19:48
 */
public interface OverflowListDao {


    /**
     * 添加商品报溢单
     * @param overflowList
     */
    void insert(OverflowList overflowList);
}
