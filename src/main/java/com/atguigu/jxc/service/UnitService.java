package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Unit;

import java.util.Map;

/**
 *
 * @author damu
 * @date 23.1.3 09:00
 */
public interface UnitService {


    /**
     * 查询所有商品单位
     * @return
     */
    Map<String, Object> getUnitList();


    /**
     * 添加商品单位
     * @param unit
     * @return
     */
    ServiceVO saveUnit(Unit unit);


    /**
     * 根据id删除商品单位
     * @param unitId
     * @return
     */
    ServiceVO deleteUnit(Integer unitId);
}
