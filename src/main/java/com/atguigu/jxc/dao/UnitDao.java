package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Unit;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品单位
 * @author damu
 * @date 22.12.30 09:51
 */
public interface UnitDao {


    /**
     * 查询所有商品单位
     * @return
     */
    List<Unit> selectAll();

    /**
     * 添加商品单位
     * @param unit
     * @return
     */
    Integer insert(@Param("unit") Unit unit);

    /**
     * 根据id删除商品单位
     * @param unitId
     * @return
     */
    Integer deleteById(Integer unitId);


}
