package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;
import com.atguigu.jxc.service.GoodsTypeService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author damu
 * @date 23.1.3 08:55
 */
@Service
public class GoodsTypeServiceImpl implements GoodsTypeService {

    @Autowired
    private GoodsTypeDao goodsTypeDao;


    /**
     * 查询商品所有分类
     * @return
     */
    @Override
    public List<GoodsTypeVo> getAllGoodsTypeTree() {
        // 创建结果集 allGoodsTypeTree
        List<GoodsTypeVo> allGoodsTypeTree = new ArrayList<>();

        // 查询所有商品分类
        List<GoodsType> allGoodsTypeList = goodsTypeDao.selectAll();
        if (CollectionUtils.isEmpty(allGoodsTypeList)) {
            // 如果查询结果为空直接返回
            return allGoodsTypeTree;
        }

        // 树化
        allGoodsTypeTree = this.toTree(allGoodsTypeList,-1);

        // 返回结果
        return allGoodsTypeTree;
    }


    /**
     * 新增商品类别
     *
     * @param goodsType
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServiceVO saveGoodsType(GoodsType goodsType) throws Exception {
        // 完善要添加的信息
        goodsType.setGoodsTypeState(0); // 类别状态, 0为叶子节点
        // 添加到数据库
        goodsTypeDao.insert(goodsType);
        // 将它的父类别状态改为 1
        goodsTypeDao.upDateStatusById(goodsType.getPId(),1);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }

    /**
     * 删除商品类别
     * @param goodsTypeId
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServiceVO deleteGoodsType(Integer goodsTypeId) {
        // 根据id查询商品类别
        GoodsType goodsType = goodsTypeDao.selectById(goodsTypeId);
        if (goodsType == null) {
            return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);// 成功
        }
        // 获取父节点id
        Integer pId = goodsType.getPId();
        // 根据id删除商品类别
        goodsTypeDao.deleteById(goodsTypeId);
        // 根据pid查询商品类别, 查看刚刚删除的节点是否还有兄弟节点
        List<GoodsType> goodsTypeList = goodsTypeDao.selectByPId(pId);
        if (CollectionUtils.isEmpty(goodsTypeList)) {
            // 没兄弟节点-->将父节点状态改为 0:叶子节点
            goodsTypeDao.upDateStatusById(pId, 0);
        }
        // 返回成功
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);// 成功
    }



    // 树化方法
    private List<GoodsTypeVo> toTree(List<GoodsType> list, Integer pId ) {
        if (CollectionUtils.isEmpty(list)) return null;
        // 创建临时集合
        List<GoodsTypeVo> TempList = new ArrayList<>();
        // 遍历传入的集合
        for (GoodsType item : list) {
            // 根据pid筛选,符合的进行封装
            if(item.getPId() == pId){
                // 创建转换成GoodsTypeVo对象
                GoodsTypeVo goodsTypeVo = new GoodsTypeVo();
                goodsTypeVo.setId(item.getGoodsTypeId());
                goodsTypeVo.setText(item.getGoodsTypeName());
                Integer attributesState = item.getGoodsTypeState();
                goodsTypeVo.setState(attributesState == 1 ? "closed" : attributesState == 0 ? "open" : null);
                goodsTypeVo.setIconCls("goods-type");
                HashMap<String, Object> map = new HashMap<>();
                map.put("state", attributesState);
                goodsTypeVo.setAttributes(map);
                // 根据它的id作为pid去找它的children进行封装
                goodsTypeVo.setChildren(this.toTree(list,item.getGoodsTypeId()));   // 递归调用
                // 添加进临时集合
                TempList.add(goodsTypeVo);
            }
        }
        // 返回临时集合
        return TempList;
    }



}
