package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.GoodsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 商品分类管理
 * @author damu
 * @date 23.1.3 08:50
 */
@RestController
@RequestMapping("/goodsType")
public class GoodsTypeController {

    @Autowired
    private GoodsTypeService goodsTypeService;


    /**
     * 查询商品所有分类
     * 请求URL：http://localhost:8080/goodsType/loadGoodsType
     * 请求参数：无
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：String
     *
     * @return
     */
    @PostMapping("/loadGoodsType")
    public List<GoodsTypeVo> getAllGoodsTypeTree() {
        return goodsTypeService.getAllGoodsTypeTree();
    }



    /**
     * 新增商品类别
     * 请求URL：http://localhost:8080/goodsType/save
     * 请求参数：String  goodsTypeName,Integer  pId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO saveGoodsType(GoodsType goodsType) throws Exception{
        return goodsTypeService.saveGoodsType(goodsType);
    }

    /**
     * 删除商品类别
     * 请求URL：http://localhost:8080/goodsType/delete
     * 请求参数：Integer  goodsTypeId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/delete")
    public  ServiceVO deleteGoodsType(Integer goodsTypeId) {
        return goodsTypeService.deleteGoodsType(goodsTypeId);
    }



}
