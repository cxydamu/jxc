package com.atguigu.jxc.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

/**
 * 商品类别(树结构)
 */
@Data
@NoArgsConstructor
public class GoodsTypeVo {

  private Integer id;
  private String text;
  private String state;
  private String iconCls;
  private Map<String,Object> attributes ;
  private List<GoodsTypeVo> children;

}