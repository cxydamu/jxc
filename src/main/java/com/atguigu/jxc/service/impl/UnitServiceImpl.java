package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Unit;
import com.atguigu.jxc.service.UnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author damu
 * @date 23.1.3 09:00
 */
@Service
public class UnitServiceImpl implements UnitService {

    @Autowired
    private UnitDao unitDao;

    /**
     * 查询所有商品单位
     * @return
     */
    @Override
    public Map<String, Object> getUnitList() {
        // 创建结果集对象
        HashMap<String, Object> resultMap = new HashMap<>();
        // 查询所有商品单位
        List<Unit> unitList = unitDao.selectAll();
        // 封装结果集
        resultMap.put("rows", unitList);
        // 返回结果集
        return resultMap;
    }


    /**
     * 添加商品单位
     * @param unit
     * @return
     */
    @Override
    public ServiceVO saveUnit(Unit unit) {
        int rowsAffected = unitDao.insert(unit);
        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }


    /**
     * 根据id删除商品单位
     * @param unitId
     * @return
     */
    @Override
    public ServiceVO deleteUnit(Integer unitId) {
        int rowsAffected = unitDao.deleteById(unitId);
        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }
}
