package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;

import java.util.List;
import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();


    /**
     * 分页查询商品库存信息
     * @param page
     * @param size
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> getInventoryPage(Integer page, Integer size, String codeOrName, Integer goodsTypeId);




    /**
     * 分页查询商品信息
     * @param page
     * @param size
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    Map<String, Object> getGoodsPage(Integer page, Integer size, String goodsName, Integer goodsTypeId);


    /**
     * 添加或修改商品信息
     * @param goods
     */
    ServiceVO saveGoods(Goods goods, Integer goodsId);


    /**
     * 删除商品信息
     * @param goodsId
     * @return
     */
    ServiceVO deleteGoodsById(Integer goodsId);


    /**
     *  分页查询有库存商品信息或无库存商品信息（可以根据商品名称或编码查询）
     * @param page
     * @param size
     * @param nameOrCode
     * @param isHasInventory 是否有库存: 1->有 ;  0->无
     * @return
     */
    Map<String, Object> getHasOrNoInventoryGoods(Integer page, Integer size, String nameOrCode, int isHasInventory);


    /**
     * 添加商品期初库存
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice);


    /**
     * 删除商品库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId
     * @return
     */
    ServiceVO deleteStockById(Integer goodsId);
}
