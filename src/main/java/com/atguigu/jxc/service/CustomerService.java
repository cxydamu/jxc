package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;

import java.util.Map;

/**
 * @author damu
 * @date 23.1.2 15:23
 */
public interface CustomerService {

    /**
     * 客户列表分页（名称模糊查询）
     * @param page
     * @param size
     * @param customerName
     * @return
     */
    Map<String, Object> getCustomerPage(Integer page, Integer size, String customerName);


    /**
     * 客户添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    ServiceVO saveCustomer(Customer customer, Integer customerId);


    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    ServiceVO deleteCustomerByIds(String ids);
}
