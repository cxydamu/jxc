package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 客户管理
 * @author damu
 * @date 23.1.2 15:19
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;


    /**
     * 客户列表分页（名称模糊查询）
     * 请求URL：http://localhost:8080/customer/list
     * 请求参数：Integer page, Integer rows, String  customerName
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("/list")
    public Map<String,Object> getCustomerPage(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                              @RequestParam(value = "rows", defaultValue = "30")Integer rows ,
                                              @RequestParam(value = "customerName", required = false)String customerName){
        return customerService.getCustomerPage(page, rows, customerName);
    }


    /**
     * 客户添加或修改
     * 请求URL：http://localhost:8080/customer/save?customerId=1
     * 请求参数：
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO saveCustomer(Customer customer,
                                  @RequestParam(value = "customerId", required = false) Integer customerId) {
        return customerService.saveCustomer(customer, customerId);
    }

    /**
     * 客户删除（支持批量删除）
     * 请求URL：http://localhost:8080/customer/delete
     * 请求参数：String  ids
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/delete")
    public ServiceVO deleteCustomerByIds(String  ids) throws Exception  {
        return customerService.deleteCustomerByIds(ids);
    }


}
