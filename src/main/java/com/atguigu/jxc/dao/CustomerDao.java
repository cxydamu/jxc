package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author damu
 * @date 23.1.2 15:24
 */
public interface CustomerDao {


    /**
     * 根据客户名称查询客户列表
     * @param customerName
     * @return
     */
    List<Customer> selectAll(@Param("customerName") String customerName);


    /**
     * 添加客户
     * @param customer
     * @return
     */
    Integer insert(Customer customer);

    /**
     * 根据id查询客户
     * @param customerId
     * @return
     */
    Customer selectById(@Param("customerId")Integer customerId);

    /**
     * 修改客户
     * @param oldCustomer
     * @return
     */
    Integer updateById(Customer oldCustomer);

    /**
     * 批量删除客户
     * @param idList
     * @return
     */
    Integer deleteByIds(@Param("idList") List<Integer> idList);
}
