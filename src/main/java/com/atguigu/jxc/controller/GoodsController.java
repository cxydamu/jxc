package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.GoodsTypeVo;
import com.atguigu.jxc.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;


    /**
     * 分页查询商品库存信息
     * 请求URL：http://localhost:8080/goods/listInventory
     * 请求参数：Integer page, Integer rows, String codeOrName, Integer goodsTypeId
     * 请求方式：POST
     * 返回值类型： JSON
     * 返回值：Map<String,Object>
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String, Object> getInventoryPage(@RequestParam(defaultValue = "1") Integer page,
                                                @RequestParam(defaultValue = "30") Integer rows,
                                                @RequestParam(required = false) String codeOrName,
                                                @RequestParam(required = false) Integer goodsTypeId) {
        return goodsService.getInventoryPage(page, rows, codeOrName, goodsTypeId);
    }



    /**
     * 分页查询商品信息
     * （可以根据分类、名称查询）
     * 请求URL：http://localhost:8080/goods/list
     * 请求参数：Integer page, Integer rows, String goodsName, Integer goodsTypeId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> getGoodsPage(@RequestParam(defaultValue = "1") Integer page,
                                            @RequestParam(defaultValue = "30") Integer rows,
                                            @RequestParam(required = false) String goodsName,
                                            @RequestParam(required = false) Integer goodsTypeId) {
        return goodsService.getGoodsPage(page, rows, goodsName, goodsTypeId);
    }


    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }


    /**
     * 添加或修改商品信息
     * 请求URL：http://localhost:8080/goods/save?goodsId=37
     * 请求参数：Goods goods
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     *
     * @param goods 商品信息实体
     * @return
     */
    @PostMapping("/save")
    public ServiceVO saveGoods(Goods goods, @RequestParam(value = "goodsId", required = false) Integer goodsId) {
        return goodsService.saveGoods(goods, goodsId);
    }


    /**
     * 删除商品信息
     * 请求URL：http://localhost:8080/goods/delete
     * 请求参数：Integer goodsId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     *
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO deleteGoodsById(@RequestParam("goodsId") Integer goodsId) {
        return goodsService.deleteGoodsById(goodsId);
    }


    /**
     * 分页查询无库存商品信息
     * 无库存商品列表展示（可以根据商品名称或编码查询）
     * 请求URL：http://localhost:8080/goods/getNoInventoryQuantity
     * 请求参数：Integer page,Integer rows,String nameOrCode
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     *
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String,Object> getNoInventoryGoods(@RequestParam(name = "page", defaultValue = "1") Integer page,
                                                  @RequestParam(name = "rows", defaultValue = "30") Integer rows,
                                                  @RequestParam(value = "nameOrCode",required = false) String nameOrCode){
        return goodsService.getHasOrNoInventoryGoods(page, rows, nameOrCode, 0);
    }


    /**
     * 分页查询有库存商品信息
     * 有库存商品列表展示（可以根据商品名称或编码查询）
     * 请求URL：http://localhost:8080/goods/getHasInventoryQuantity
     * 请求参数：Integer page,Integer rows,String nameOrCode
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：Map<String,Object>
     *
     * @param page 当前页
     * @param rows 每页显示条数
     * @param nameOrCode 商品名称或商品编码
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String,Object> getHasInventoryGoods(@RequestParam(name = "page", defaultValue = "1") Integer page,
                                                   @RequestParam(name = "rows", defaultValue = "30") Integer rows,
                                                   @RequestParam(value = "nameOrCode",required = false) String nameOrCode){
        return goodsService.getHasOrNoInventoryGoods(page, rows, nameOrCode, 1);
    }


    /**
     * 添加商品期初库存
     * 添加库存、修改数量或成本价
     * 请求URL：http://localhost:8080/goods/saveStock?goodsId=25
     * 请求参数：Integer goodsId,Integer inventoryQuantity,double purchasingPrice
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     *
     * @param goodsId           商品ID
     * @param inventoryQuantity 库存
     * @param purchasingPrice   成本价
     * @return
     */
    @PostMapping("/saveStock")
    public ServiceVO saveStock(@RequestParam(value = "goodsId",required = false) Integer goodsId,
                               @RequestParam(value = "inventoryQuantity")Integer inventoryQuantity,
                               @RequestParam(value = "purchasingPrice")double purchasingPrice) {
        return goodsService.saveStock(goodsId, inventoryQuantity, purchasingPrice);
    }





    /**
     * 删除商品库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     * 请求URL：http://localhost:8080/goods/deleteStock
     * 请求参数：Integer goodsId
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     * @param goodsId 商品ID
     * @return
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStockById(@RequestParam(value = "goodsId") Integer goodsId) {
        return goodsService.deleteStockById(goodsId);
    }







    /**
     * 查询库存报警商品信息
     * @return
     */

}
