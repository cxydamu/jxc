package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    /**
     * 带条件查询库存信息
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    List<Goods> selectInventoryGoodsList(@Param(value = "codeOrName") String codeOrName,
                                         @Param(value = "goodsTypeId") Integer goodsTypeId);


    /**
     * 待条件查询商品信息
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    List<Goods> getGoodsList(@Param(value = "goodsName") String goodsName,
                             @Param(value = "goodsTypeId") Integer goodsTypeId);


    /**
     * 根据id查询商品
     * @param goodsId
     * @return
     */
    Goods selectById(Integer goodsId);

    /**
     * 添加商品信息
     * @param goods
     */
    Integer insert(@Param("goods") Goods goods);


    /**
     * 修改商品信息
     * @param goods
     */
    Integer updateById(@Param("goods")Goods goods);


    /**
     * 删除商品信息
     * @param goodsId
     */
    Integer deleteById(Integer goodsId);


    /**
     * 带条件查询无库存商品信息
     * @param nameOrCode
     * @return
     */
    List<Goods> selectHasOrNoInventoryGoods(@Param("nameOrCode") String nameOrCode, @Param("isHasInventory")int isHasInventory);


    /**
     * 修改商品库存信息
     * @param goods
     */
    Integer updateInventoryQuantityAndPurchasingPriceById(Goods goods);
}
