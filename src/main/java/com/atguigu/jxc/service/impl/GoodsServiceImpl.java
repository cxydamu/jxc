package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.GoodsTypeDao;
import com.atguigu.jxc.dao.UnitDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @description
 */
@Service
@SuppressWarnings("all")
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Autowired
    private GoodsTypeDao goodsTypeDao;




    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品库存信息
     * @param page
     * @param size
     * @param codeOrName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> getInventoryPage(Integer page, Integer size, String codeOrName, Integer goodsTypeId) {
        // 创建返回结果集对象
        Map<String, Object> resultMap = new HashMap<>();

        // 带条件查询库存信息
        List<Goods> InventoryList = goodsDao.selectInventoryGoodsList(codeOrName,goodsTypeId);
        if (CollectionUtils.isEmpty(InventoryList)) {
            // 如果查询结果为空直接返回
            return resultMap;
        }

        // 创建返回值
        int total = InventoryList.size();   // 总条数

        // 计算当前页下标
        int startIndex = (page - 1) * size;                                  // 起始下表(包含)
        int endIndex = startIndex+size <= total ? startIndex+size : total;   // 终止下标(不包含)
        // 获取当前页数据列表 -- 手动分页
        List<Goods> rows = InventoryList.subList(startIndex,endIndex); // 商品库存信息列表

        // 封装结果返回
        resultMap.put("total", total);  // 封装总条数
        resultMap.put("rows", rows);    // 封装商品库存信息列表
        // 返回结果集
        return resultMap;
    }





    /**
     * 分页查询商品信息
     * @param page
     * @param size
     * @param goodsName
     * @param goodsTypeId
     * @return
     */
    @Override
    public Map<String, Object> getGoodsPage(Integer page, Integer size, String goodsName, Integer goodsTypeId) {
        // 创建返回结果集对象
        Map<String, Object> resultMap = new HashMap<>();

        // 待条件查询商品信息
        List<Goods> goodsList = goodsDao.getGoodsList(goodsName,goodsTypeId);
        if (CollectionUtils.isEmpty(goodsList)) {
            // 如果查询结果为空直接返回
            return resultMap;
        }

        // 创建返回值
        int total = goodsList.size();   // 总条数



//        // 计算当前页下标
//        int startIndex = (page - 1) * size;                                  // 起始下表(包含)
//        int endIndex = startIndex+size <= total ? startIndex+size : total;   // 终止下标(不包含)
//        // 获取当前页数据列表
//        List<Goods> rows = goodsList.subList(startIndex,endIndex); // 商品库存信息列表



        // 获取当前页数据列表
        List<Goods> rows = goodsList.stream().skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());


        // 封装结果返回
        resultMap.put("total", total);  // 封装总条数
        resultMap.put("rows", rows);    // 封装商品库存信息列表
        return resultMap;
    }



    /**
     * 添加或修改商品信息
     * @param goods
     * @param goodsId
     */
    @Override
    public ServiceVO saveGoods(Goods goods, Integer goodsId) {
        int rowsAffected = 0 ;
        if (goodsId == null) {
            // 添加操作 --数据库中不存在该商品
            // 完善要添加的数据
            goods.setInventoryQuantity(0); // 库存数量
            goods.setLastPurchasingPrice(goods.getPurchasingPrice()); // 上一次采购价格: 默认第一次
            goods.setState(0); // 状态: 0表示初始值, 1表示已入库，2表示有进货或销售单据
            if (goods.getGoodsTypeId() == null) {
                return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
            }
            // 根据id查询商品类别 goodsType 获取 商品类别名称 goods_type_name
            GoodsType goodsType = goodsTypeDao.selectById(goods.getGoodsTypeId());
            goods.setGoodsTypeName(goodsType.getGoodsTypeName());// 商品类别名称
            // 插入数据库
            rowsAffected = goodsDao.insert(goods);
        }else {
            // 修改操作 --数据库中存在该商品
            // 根据id查询对应商品
            Goods oldGoods = goodsDao.selectById(goodsId);
            // 更新字段数据
            oldGoods.setLastPurchasingPrice(oldGoods.getPurchasingPrice()); // 上一次采购价格
            oldGoods.setGoodsTypeId(goods.getGoodsTypeId()); //商品类别
            oldGoods.setGoodsName(goods.getGoodsName());   //商品名称
            oldGoods.setGoodsModel(goods.getGoodsModel());  //商品型号
            oldGoods.setGoodsUnit(goods.getGoodsUnit());    //商品单位
            oldGoods.setPurchasingPrice(goods.getPurchasingPrice()); //采购价格
            oldGoods.setSellingPrice(goods.getSellingPrice());  //销售价格
            oldGoods.setMinNum(goods.getMinNum());  //库存下限
            oldGoods.setGoodsProducer(goods.getGoodsProducer()); //生产厂商
            oldGoods.setRemarks(goods.getRemarks());    //备注
            // 根据id更新商品信息
            rowsAffected = goodsDao.updateById(oldGoods);
        }
        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);// 成功
    }


    /**
     * 删除商品信息
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO deleteGoodsById(Integer goodsId) {
        int rowsAffected = goodsDao.deleteById(goodsId);
        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);// 成功
    }


    /**
     *  分页查询有库存商品信息或无库存商品信息（可以根据商品名称或编码查询）
     * @param page
     * @param size
     * @param nameOrCode
     * @param isHas 是否有库存: 1->有 ;  0->无
     * @return
     */
    @Override
    public Map<String, Object> getHasOrNoInventoryGoods(Integer page, Integer size, String nameOrCode, int isHasInventory) {
        // 创建结果集对象
        Map<String, Object> resultMap = new HashMap<>();
        // 带条件查询无库存商品信息
        List<Goods> noInventoryGoodsList = goodsDao.selectHasOrNoInventoryGoods(nameOrCode, isHasInventory);
        if (CollectionUtils.isEmpty(noInventoryGoodsList)) {
            // 如果查询结果为空直接返回
            return resultMap;
        }
        // 获取总数量
        int total = noInventoryGoodsList.size();
        // 获取数据列表 -- 手动分页
        List<Goods> rows = noInventoryGoodsList.stream().skip((page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
        // 封装结果集
        resultMap.put("total", total);
        resultMap.put("rows", rows);
        // 返回结果集
        return resultMap;
    }


    /**
     * 添加商品期初库存
     * 添加库存、修改数量或成本价
     * @param goodsId
     * @param inventoryQuantity
     * @param purchasingPrice
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public ServiceVO saveStock(Integer goodsId, Integer inventoryQuantity, double purchasingPrice) {
        // 校验参数合法性
        if (inventoryQuantity < 0 && purchasingPrice < 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 根据id查询商品信息
        Goods goods = goodsDao.selectById(goodsId);
        if (goods == null) {
            // 如果没有该商品则返回失败:参数异常
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 跟新信息
        goods.setLastPurchasingPrice(goods.getPurchasingPrice()); // 更新上次采购价格
        goods.setInventoryQuantity(inventoryQuantity);  // 更新库存数量
        goods.setPurchasingPrice(purchasingPrice);      // 更新采购价格
        // 修改商品库存信息
        Integer rowsAffected = goodsDao.updateInventoryQuantityAndPurchasingPriceById(goods);
        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 返回成功
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);// 成功
    }


    /**
     * 删除商品库存（要判断商品状态 入库、有进货和销售单据的不能删除）
     * @param goodsId
     * @return
     */
    @Override
    public ServiceVO deleteStockById(Integer goodsId) {
        Goods goods = goodsDao.selectById(goodsId);
        if (goods == null) {
            // 如果没有该商品则返回失败:参数异常
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 获取商品当前状态
        Integer state = goods.getState();
        if (state == 1)  {
            // 该商品已入库，不能删除
            return new ServiceVO(ErrorCode.STORED_ERROR_CODE,ErrorCode.STORED_ERROR_MESS);// 失败
        } else if (state == 2) {
            // 该商品有进货或销售单据，不能删除
            return new ServiceVO(ErrorCode.HAS_FORM_ERROR_CODE,ErrorCode.HAS_FORM_ERROR_MESS);// 失败
        }
        // 将商品库存置零
        goods.setInventoryQuantity(0);
        // 删除商品库存
        Integer rowsAffected = goodsDao.updateInventoryQuantityAndPurchasingPriceById(goods);
        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE,ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 返回成功
        return new ServiceVO(SuccessCode.SUCCESS_CODE,SuccessCode.SUCCESS_MESS);// 成功
    }
}
