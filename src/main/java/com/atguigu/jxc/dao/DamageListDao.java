package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;

/**
 * 商品报损单管理
 * @author damu
 * @date 23.1.3 16:52
 */
public interface DamageListDao {


    /**
     * 添加商品报损单
      * @param damageList
     */
    void insert(DamageList damageList);
}
