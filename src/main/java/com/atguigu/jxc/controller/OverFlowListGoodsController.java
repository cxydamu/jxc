package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.*;
import com.atguigu.jxc.service.OverFlowListGoodsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.util.List;

/**
 * @author damu
 * @date 23.1.3 19:34
 */
@RestController
@RequestMapping("/overflowListGoods")
public class OverFlowListGoodsController {

    @Autowired
    private OverFlowListGoodsService overFlowListGoodsService;

    /**
     * 新增报溢单
     * 请求URL：http://localhost:8080/overflowListGoods/save?overflowNumber=BY1605767033015（报溢单号）
     * 请求参数：OverflowList overflowList, String overflowListGoodsStr
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
        public ServiceVO saveOverflowList(@PathParam("overflowList") OverflowList overflowList,
                                          @PathParam("overflowListGoodsStr") String overflowListGoodsStr,
                                          HttpSession session) {
            User user = (User) session.getAttribute("currentUser");
            if (user == null) {
                // 返回失败: 登录已过期, 请重新登录
                return new ServiceVO(ErrorCode.LOGIN_EXPIRE_CODE, ErrorCode.LOGIN_EXPIRE_MESS);
            }
            // 完善用户id
            overflowList.setUserId(user.getUserId());
            // 将JSON字符串转换成 List<DamageListGoods> 集合
            List<OverflowListGoods> overflowListGoodsList = new Gson().fromJson(overflowListGoodsStr, new TypeToken<List<OverflowListGoods>>() {
            }.getType());
            // 调用service层保存方法
            return overFlowListGoodsService.saveOverflowList(overflowList, overflowListGoodsList);
        }
}
