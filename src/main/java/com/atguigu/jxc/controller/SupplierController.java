package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.hibernate.validator.constraints.ParameterScriptAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.util.Map;

/**
 * 供应商管理
 *
 * @author damu
 * @date 22.12.30 15:45
 */
@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService;


    /**
     * 分页查询供应商
     * 请求URL：http://localhost:8080/supplier/list
     * 请求参数：Integer page（当前页数）, Integer rows（每页显示的记录数）, String supplierName
     * 请求方式：POST
     * 返回值类型： JSON
     * 返回值：Map<String,Object>
     */
    @PostMapping("/list")
    public Map<String,Object> getSupplierPage(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                              @RequestParam(value = "rows", defaultValue = "30")Integer rows ,
                                              @RequestParam(value = "supplierName", required = false)String supplierName){
        return supplierService.getSupplierPage(page, rows, supplierName);
    }

    /**
     * 供应商添加或修改
     * 请求URL：http://localhost:8080/supplier/save?supplierId=1
     * 请求参数：Supplier supplier
     * 请求方式：POST
     * 返回值类型： JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO saveSupplier(Supplier supplier,
                                  @RequestParam(value = "supplierId", required = false) Integer supplierId) {
        return supplierService.saveSupplier(supplier, supplierId);
    }


    /**
     * 删除供应商（支持批量删除）
     * 请求URL：http://localhost:8080/supplier/delete
     * 请求参数：String  ids
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/delete")
    public ServiceVO deleteSupplierByIds(String  ids) throws Exception  {
        return supplierService.deleteSupplierByIds(ids);
    }


}
