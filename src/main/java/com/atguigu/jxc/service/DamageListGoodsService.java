package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

/**
 * 库存管理
 * @author damu
 * @date 23.1.3 16:50
 */
public interface DamageListGoodsService {


    /**
     * 保存报损单
     * @param damageList
     * @param damageListGoodsList
     * @return
     */
    ServiceVO saveDamageList(DamageList damageList, List<DamageListGoods> damageListGoodsList);
}
