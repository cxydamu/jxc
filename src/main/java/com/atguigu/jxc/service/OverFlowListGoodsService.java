package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

/**
 * @author damu
 * @date 23.1.3 19:41
 */
public interface OverFlowListGoodsService {


    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsList
     * @return
     */
    ServiceVO saveOverflowList(OverflowList overflowList, List<OverflowListGoods> overflowListGoodsList);
}
