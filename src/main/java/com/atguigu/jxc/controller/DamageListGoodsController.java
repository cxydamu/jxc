package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.User;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.shiro.web.session.HttpServletSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * 库存管理
 * @author damu
 * @date 23.1.3 16:26
 */
@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService;


    /**
     * 保存报损单
     * 请求URL：http://localhost:8080/damageListGoods/save?damageNumber=BS1605766644460（报损单号,前端生成）
     * 请求参数：DamageList damageList, String damageListGoodsStr
     * 请求方式：POST
     * 返回值类型：JSON
     * 返回值：ServiceVO
     */
    @PostMapping("/save")
    public ServiceVO saveDamageList(@PathParam("damageList") DamageList damageList,
                                         @PathParam("damageListGoodsStr") String damageListGoodsStr,
                                         HttpSession session) {
        User user = (User) session.getAttribute("currentUser");
        if (user == null) {
            // 返回失败: 登录已过期, 请重新登录
            return new ServiceVO(ErrorCode.LOGIN_EXPIRE_CODE, ErrorCode.LOGIN_EXPIRE_MESS);
        }
        // 完善商品用户id
        damageList.setUserId(user.getUserId());
        // 将JSON字符串转换成 List<DamageListGoods> 集合
        List<DamageListGoods> damageListGoodsList = new Gson().fromJson(damageListGoodsStr, new TypeToken<List<DamageListGoods>>() {
        }.getType());
        // 调用service层保存方法
        return damageListGoodsService.saveDamageList(damageList, damageListGoodsList);
    }


}
