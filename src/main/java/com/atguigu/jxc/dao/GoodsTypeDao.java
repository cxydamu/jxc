package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 商品类别
 */
public interface GoodsTypeDao {


    Integer updateGoodsTypeState(GoodsType parentGoodsType);


    /**
     * 查询商品所有商品类别
     * @return
     */
    ArrayList<GoodsType> selectAll();


    /**
     * 根据id查询商品类别
     * @param goodsTypeId
     * @return
     */
    GoodsType selectById(Integer goodsTypeId);

    /**
     * 新增商品类别
     * @param goodsType
     * @return
     */
    void insert(@Param("goodsType") GoodsType goodsType);


    /**
     * 修改商品类别状态
     * @param id
     * @param newStatus
     */
    Integer upDateStatusById(@Param("id") Integer id, @Param("newStatus") Integer newStatus);


    /**
     * 删除商品类别
     * @param goodsTypeId
     */
    Integer deleteById(Integer goodsTypeId);


    /**
     * 根据pid查询商品类别
     * @param pId
     */
    List<GoodsType> selectByPId(@Param("pId") Integer pId);
}
