package com.atguigu.jxc.util;

import com.atguigu.jxc.entity.Customer;
import org.apache.commons.collections.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 分页工具类
 * @author damu
 * @date 23.1.3 15:39
 */
public class PageUtil {



    public static Map<String, Object> getPageMap(List<Object> list, Integer page, Integer size){
        // 创建返回结果集
        Map<String, Object> resultMap = new HashMap<>();
        if (CollectionUtils.isEmpty(list)) {
            return resultMap;
        }
        // 获取总数量
        int total = list.size();
        // 获取当前页数据--手动分页
        List<Object> rows = list.stream().skip((page - 1) * size).limit(size).collect(Collectors.toList());
        // 封装结果集
        resultMap.put("total", total);
        resultMap.put("rows", rows);
        // 返回结果集
        return resultMap;
    }

}
