package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowListGoods;

/**
 * 商品报溢单商品管理
 * @author damu
 * @date 23.1.3 19:49
 */
public interface OverflowListGoodsDao {

    /**
     * 添加商品报溢单商品
     * @param overflowListGoods
     */
    void insert(OverflowListGoods overflowListGoods);
}
