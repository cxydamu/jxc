package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.service.CustomerService;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author damu
 * @date 23.1.2 15:23
 */
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDao customerDao;


    /**
     * 客户列表分页（名称模糊查询）
     * @param page
     * @param size
     * @param customerName
     * @return
     */
    @Override
    public Map<String, Object> getCustomerPage(Integer page, Integer size, String customerName) {
        // 创建返回结果集
        Map<String, Object> resultMap = new HashMap<>();
        // 根据条件模糊查询(条件为空则查询所有)
        List<Customer> customerList = customerDao.selectAll(customerName);
        if (CollectionUtils.isEmpty(customerList)) {
            return resultMap;
        }

        // 获取总数量
        int total = customerList.size();
        // 获取当前页数据--手动分页
        List<Customer> rows = customerList.stream().skip((page - 1) * size)
                                                      .limit(size)
                                                      .collect(Collectors.toList());
        // 封装结果集
        resultMap.put("total", total);
        resultMap.put("rows", rows);
        // 返回结果集
        return resultMap;
    }


    /**
     * 客户添加或修改
     * @param customer
     * @param customerId
     * @return
     */
    @Override
    public ServiceVO saveCustomer(Customer customer, Integer customerId) {
        int RowsAffected = 0;
        if (customerId == null) {
            // 添加操作 --数据库中不存在该客户
            // 插入数据库
            RowsAffected = customerDao.insert(customer);
        } else {
            // 修改操作 --数据库中存在该客户

            // 根据id查询对应客户
            Customer oldCustomer = customerDao.selectById(customerId);
            // 更新字段数据
            oldCustomer.setCustomerName(customer.getCustomerName()); // 客户名称
            oldCustomer.setContacts(customer.getContacts());  //客户姓名
            oldCustomer.setPhoneNumber(customer.getPhoneNumber()); //客户电话
            oldCustomer.setAddress(customer.getAddress());   // 客户地址
            oldCustomer.setRemarks(customer.getRemarks());   // 备注
            // 根据id更新客户信息
            RowsAffected = customerDao.updateById(oldCustomer);
        }

        if (RowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }


    /**
     * 客户删除（支持批量删除）
     * @param ids
     * @return
     */
    @Override
    public ServiceVO deleteCustomerByIds(String ids) {
        // 创建一个List集合用于接收id
        List<Integer> idList = new ArrayList<>();
        // 数据校验 传入的ids 应该类似于 "3,5,8,10"
        try {
            // 尝试将ids转换成List集合,如果转换失败则说明参数有误,是非法调用
            String[] strIdList = ids.split(",");
            for (String id : strIdList) {
                idList.add(Integer.parseInt(id));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 删除数据库中的数据
        int RowsAffected = customerDao.deleteByIds(idList);
        // 结果校验
        if (RowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 返回成功
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }


}
