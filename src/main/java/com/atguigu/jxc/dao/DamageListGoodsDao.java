package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;

/**
 * 商品报损单商品列表管理
 * @author damu
 * @date 23.1.3 16:53
 */
public interface DamageListGoodsDao {

    /**
     * 添加商品报损单商品列表
     * @param damageListGoods
     */
    void insert(DamageListGoods damageListGoods);

}
