package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

/**
 * @author damu
 * @date 22.12.30 16:15
 */
public interface SupplierService {


    /**
     * 分页查询供应商
     * @param page
     * @param size
     * @param supplierName
     * @return
     */
    Map<String, Object> getSupplierPage(Integer page, Integer size, String supplierName);


    /**
     * 供应商添加或修改
     * @param supplier
     * @param supplierId
     * @return
     */
    ServiceVO saveSupplier(Supplier supplier, Integer supplierId);


    /**
     * 删除供应商（支持批量删除）
     * @param ids
     * @return
     */
    ServiceVO deleteSupplierByIds(String ids) throws Exception ;
}
