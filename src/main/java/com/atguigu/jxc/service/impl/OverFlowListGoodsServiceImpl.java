package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.OverFlowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author damu
 * @date 23.1.3 19:41
 */
@Service
public class OverFlowListGoodsServiceImpl implements OverFlowListGoodsService {

    @Autowired
    private OverflowListDao overflowListDao;

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao;


    /**
     * 新增报溢单
     * @param overflowList
     * @param overflowListGoodsList
     * @return
     */
    @Override
    public ServiceVO saveOverflowList(OverflowList overflowList, List<OverflowListGoods> overflowListGoodsList) {
        // 添加商品报溢单
        overflowListDao.insert(overflowList);
        // 添加商品报溢单商品列表
        overflowListGoodsList.forEach(overflowListGoods -> {
            // 完善商品报溢单id
            overflowListGoods.setOverflowListId(overflowList.getOverflowListId());
            // 添加商品报溢单商品
            overflowListGoodsDao.insert(overflowListGoods);
        });
        // 返回成功
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }
}
