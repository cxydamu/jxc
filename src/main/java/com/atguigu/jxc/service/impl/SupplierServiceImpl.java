package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.GoodsType;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author damu
 * @date 22.12.30 16:16
 */
@Service
@SuppressWarnings("all")
public class SupplierServiceImpl implements SupplierService {

    @Autowired
    private SupplierDao supplierDao;


    /**
     * 分页查询供应商
     *
     * @param page
     * @param size
     * @param supplierName
     * @return
     */
    @Override
    public Map<String, Object> getSupplierPage(Integer page, Integer size, String supplierName) {
        // 创建结果集对象
        HashMap<String, Object> resultMap = new HashMap<>();
        // 查询所有
        List<Supplier> allList = supplierDao.selectBySupplierName(supplierName);
        // 非空校验
        if (CollectionUtils.isEmpty(allList)) {
            return resultMap;
        }
        // 获取总数量
        int total = allList.size();
        // 获取当前页数据
        List<Supplier> rows = allList.stream().skip((page - 1) * size).limit(size).collect(Collectors.toList());
        // 封装结果集
        resultMap.put("total", total);
        resultMap.put("rows", rows);
        // 返回结果集
        return resultMap;
    }


    /**
     * 供应商添加或修改
     *
     * @param supplier
     * @param supplierId
     * @return
     */
    @Override
    public ServiceVO saveSupplier(Supplier supplier, Integer supplierId) {
        int rowsAffected = 0;
        if (supplierId == null) {
            // 添加操作 --数据库中不存在该供应商
            // 插入数据库
            rowsAffected = supplierDao.insert(supplier);
        } else {
            // 修改操作 --数据库中存在该供应商

            // 根据id查询对应供应商
            Supplier oldSupplier = supplierDao.selectById(supplierId);
            // 更新字段数据
            oldSupplier.setSupplierName(supplier.getSupplierName()); // 供应商名称
            oldSupplier.setContacts(supplier.getContacts());  //联系人姓名
            oldSupplier.setPhoneNumber(supplier.getPhoneNumber()); //联系人电话
            oldSupplier.setAddress(supplier.getAddress());   // 供应商地址
            oldSupplier.setRemarks(supplier.getRemarks());   // 备注
            // 根据id更新供应商信息
            rowsAffected = supplierDao.updateById(oldSupplier);
        }

        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }


    /**
     * 删除供应商（支持批量删除）
     *
     * @param ids
     * @return
     */
    @Override
    public ServiceVO deleteSupplierByIds(String ids) throws Exception {
        // 数据校验 传入的ids 应该类似于 "3,5,8,10"
        try {
            // 尝试将其中数字转换成int类型,如果转换失败则说明参数有误,是非法调用
            String[] idList = ids.split(",");
            for (String id : idList) {
                int i = Integer.parseInt(id);
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 删除数据库中的数据
        int rowsAffected = supplierDao.deleteByIds(ids);
        // 结果校验
        if (rowsAffected <= 0) {
            return new ServiceVO(ErrorCode.PARA_TYPE_ERROR_CODE, ErrorCode.PARA_TYPE_ERROR_MESS);// 失败
        }
        // 返回成功
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);// 成功
    }
}
